import React from "react"
import './App.css';
import {Route, Switch} from "react-router-dom";
import Employees from "./components/Employees/Employees";
import Main from "./components/Main";
import Navbar from "./components/Navbar";

function App() {
    return (
        <>
            <Navbar/>
            <Switch>
                <Route path="/" exact component={Main}/>
                <Route path="/employees" component={Employees}/>
            </Switch>
        </>
    );
}

export default App;
