import {NavLink} from "react-router-dom";

const Navbar = () => {
    return (
        <ul className="nav">
            <li>
                <NavLink to="/">Главная</NavLink>
            </li>
            <li>
                <NavLink to="/employees">Сотрудники</NavLink>
            </li>
        </ul>
    )
}

export default Navbar
