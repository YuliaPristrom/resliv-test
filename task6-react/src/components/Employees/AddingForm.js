import React, {useState} from "react"

const AddingForm = ({addEmployee}) => {
    const [newEmployee, setNewEmployee] = useState("")

    const addStaff = (e) => {
        e.preventDefault()
        const employee = {
            id: newEmployee,
            first_name: newEmployee
        }
        addEmployee(employee)
        setNewEmployee('')
    }

    return (
        <form onSubmit={addStaff}>
            <input
                value={newEmployee}
                placeholder="Новый сотрудник"
                onChange={(e) => {
                    setNewEmployee(e.target.value)
                }}
            />
            <button type="submit">Добавить</button>
        </form>
    )
}

export default AddingForm
