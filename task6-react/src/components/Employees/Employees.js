import React, {useEffect, useState} from "react"
import AddingForm from "./AddingForm";

const Employees = () => {
    const [employees, setEmployees] = useState([])

    useEffect(() => {
        fetch("https://reqres.in/api/users?per_page=12")
            .then((res) => {
                if (res.ok) {
                    return res.json();
                } else {
                    return res.json()
                        .then((data) => {
                            throw new Error(data.message);
                        });
                }
            })
            .then((data) => {
                const employeesArr = data.data
                setEmployees(employeesArr)
            })
            .catch(err => {
                console.log(err.message)
            });

    }, [])

    const deleteEmployee = (id) => {
        console.log("deleteEmployee")
        const updatedSEmployees = employees.filter(item => item.id !== id)
        setEmployees(updatedSEmployees)
    }

    const addEmployee = (employee) => {
        setEmployees([employee, ...employees])
    }

    return (
        <div>
            <h2>Сотрудники</h2>
            <AddingForm addEmployee={addEmployee}/>
            <ol>
                {employees.map(item => (
                        <li key={item.id}>
                            {item.first_name}
                            <button onClick={() => deleteEmployee(item.id)}>x</button>
                        </li>
                    )
                )}
            </ol>
        </div>
    )
}

export default Employees
